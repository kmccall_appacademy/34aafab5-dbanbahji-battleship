require_relative 'player'
require_relative 'board'

class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @player = player
    @board = board
  end

  def count
    board.count
  end

  def attack(pos)
    @board[pos] = :x
  end

  def play_turn
    puts "What's your move?"
    pos = player.get_play
    attack(pos)
  end

  def game_over?
    board.won?
  end
end
